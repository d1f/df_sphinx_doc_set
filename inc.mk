# Makefile for Sphinx documentation
#

#+ Project-specific parameters
#
pkg ?= Package

# The short X.Y version.
version ?= ""

  project ?= ""
copyright ?= ""
   author ?= ""

# ru en zh_CN
lang ?= ru

source_encoding ?= utf-8-sig

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
 html_favicon ?= "" # None
 html_logo    ?= "" # None
latex_logo    ?= "" # None

paper ?= a4 # a4 | letter

src_dir ?= .
sub_dir ?= df_sphinx_doc_set
#
#- Project-specific parameters


PKG = $(pkg)-doc-$(lang)

# You can set these variables from the command line.
SPHINXOPTS    = -D pkg=$(pkg)
SPHINXOPTS   += -D version=$(version)
SPHINXOPTS   += -D language=$(lang)
SPHINXOPTS   += -D project=$(project) -D copyright=$(copyright)
SPHINXOPTS   += -D author=$(author)
SPHINXOPTS   += -D source_encoding=$(source_encoding)
SPHINXOPTS   += -D html_favicon=$(html_favicon)
SPHINXOPTS   += -D html_logo=$(html_logo)
SPHINXOPTS   += -D latex_logo=$(latex_logo)
SPHINXOPTS   += -D latex_paper_size=$(paper)

SPHINXBUILD   = sphinx-build
BUILDDIR      = ./build.$(lang)
SOURCEDIR     = $(src_dir)

# -D <setting=value> # Override a setting from the configuration file.
ALLSPHINXOPTS   = -d $(BUILDDIR)/doctrees $(SPHINXOPTS) $(SOURCEDIR)

# the i18n builder cannot share the environment and doctrees with the others
I18NSPHINXOPTS  = $(SPHINXOPTS) $(SOURCEDIR)

.PHONY: help clean html dirhtml singlehtml pickle json htmlhelp qthelp devhelp epub latex latexpdf text man changes linkcheck doctest gettext
.PHONY: all %-dist distclean


TAR := $(if $(shell which gtar),gtar,tar)
SED := $(if $(shell which gsed),gsed,sed)


all: html

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  html       to make standalone HTML files"
	@echo "  dirhtml    to make HTML files named index.html in directories"
	@echo "  singlehtml to make a single large HTML file"
	@echo "  pickle     to make pickle files"
	@echo "  json       to make JSON files"
	@echo "  htmlhelp   to make HTML files and a HTML help project"
	@echo "  qthelp     to make HTML files and a qthelp project"
	@echo "  devhelp    to make HTML files and a Devhelp project"
	@echo "  epub       to make an epub"
	@echo "  latex      to make LaTeX files, you can set PAPER=a4 or PAPER=letter"
	@echo "  latexpdf   to make LaTeX files and run them through pdflatex"
	@echo "  text       to make text files"
	@echo "  man        to make manual pages"
	@echo "  texinfo    to make Texinfo files"
	@echo "  info       to make Texinfo files and run them through makeinfo"
	@echo "  gettext    to make PO message catalogs"
	@echo "  changes    to make an overview of all changed/added/deprecated items"
	@echo "  linkcheck  to check all external links for integrity"
	@echo "  doctest    to run all doctests embedded in the documentation (if enabled)"
	@echo "  %-dist     to make distributable tar.gz"


.PHONY: links links-clean
LINKS = conf.py settings.py _static
links: links-clean
	@echo -n "Making symlinks ... "
	@mkdir -p $(SOURCEDIR)/_ext
	@for l in $(LINKS); do \
		ln -sf $(sub_dir)/$$l   $(SOURCEDIR)/$$l; \
	done
	@ln -sf ../$(sub_dir)/_ext/conf $(SOURCEDIR)/_ext/conf
	@ln -sf    $(sub_dir)/gitignore $(SOURCEDIR)/.gitignore
	@echo done

links-clean:
	@echo -n "Removing symlinks ... "
	@for l in $(LINKS); do \
		rm  -f                  $(SOURCEDIR)/$$l; \
	done
	@rm  -f                         $(SOURCEDIR)/_ext/conf
	@rm  -f                         $(SOURCEDIR)/.gitignore
	@echo done


clean distclean: links-clean
	@echo -n "Cleaning ... "
	@-rm -rf $(basename $(BUILDDIR)).*
	@find $(SOURCEDIR) -name \*.rst.\* -exec rm -f '{}' ';'
	@echo done


RST := $(wildcard $(SOURCEDIR)/*.rst)
RST += $(wildcard $(SOURCEDIR)/*/*.rst)

RST_LANG  = $(addsuffix .$(lang),$(RST))

%.rst.$(lang) : %.rst
	@echo "Splitting $< by languages ..."
	@$(sub_dir)/langsplit $< 2>/dev/null
    ifneq ($(LABEL),SIGRAND)
	@echo "Labeling by $(LABEL)"
	@$(SED) -i -e 's/Sigrand/$(Label)/g; s/Сигранд/$(LabelRu)/g' $@
	#; s/SG/$(LBL)/g
    endif

DEPS  = links
DEPS += $(RST) $(RST_LANG)
DEPS += $(SOURCEDIR)/$(sub_dir)/inc.mk
DEPS += $(SOURCEDIR)/$(sub_dir)/conf.py
DEPS += $(SOURCEDIR)/$(sub_dir)/settings.py
DEPS += $(SOURCEDIR)/Makefile

html:	$(DEPS)
	$(SPHINXBUILD) -b html $(ALLSPHINXOPTS) $(BUILDDIR)/html/$(PKG)
	@echo
	@echo "Build finished. The HTML pages are in $(BUILDDIR)/html/$(PKG)."

dirhtml: $(DEPS)
	$(SPHINXBUILD) -b dirhtml $(ALLSPHINXOPTS) $(BUILDDIR)/dirhtml/$(PKG)
	@echo
	@echo "Build finished. The HTML pages are in $(BUILDDIR)/dirhtml/$(PKG)."

singlehtml: $(DEPS)
	$(SPHINXBUILD) -b singlehtml $(ALLSPHINXOPTS) $(BUILDDIR)/singlehtml/$(PKG)
	@echo
	@echo "Build finished. The HTML page is in $(BUILDDIR)/singlehtml/$(PKG)."

pickle:	$(DEPS)
	$(SPHINXBUILD) -b pickle $(ALLSPHINXOPTS) $(BUILDDIR)/pickle/$(PKG)
	@echo
	@echo "Build finished; now you can process the pickle files."

json:	$(DEPS)
	$(SPHINXBUILD) -b json $(ALLSPHINXOPTS) $(BUILDDIR)/json/$(PKG)
	@echo
	@echo "Build finished; now you can process the JSON files."

htmlhelp: $(DEPS)
	$(SPHINXBUILD) -b htmlhelp $(ALLSPHINXOPTS) $(BUILDDIR)/htmlhelp/$(PKG)
	@echo
	@echo "Build finished; now you can run HTML Help Workshop with the" \
	      ".hhp project file in $(BUILDDIR)/htmlhelp/$(PKG)."

qthelp:	$(DEPS)
	$(SPHINXBUILD) -b qthelp $(ALLSPHINXOPTS) $(BUILDDIR)/qthelp/$(PKG)
	@echo
	@echo "Build finished; now you can run "qcollectiongenerator" with the" \
	      ".qhcp project file in $(BUILDDIR)/qthelp/$(PKG), like this:"
	@echo "# qcollectiongenerator $(BUILDDIR)/qthelp/$(PKG)/$(PKG).qhcp"
	@echo "To view the help file:"
	@echo "# assistant -collectionFile $(BUILDDIR)/qthelp/$(PKG)/$(PKG).qhc"

devhelp: $(DEPS)
	$(SPHINXBUILD) -b devhelp $(ALLSPHINXOPTS) $(BUILDDIR)/devhelp/$(PKG)
	@echo
	@echo "Build finished."
	@echo "To view the help file:"
	@echo "# mkdir -p $$HOME/.local/share/devhelp/$(PKG)"
	@echo "# ln -s $(BUILDDIR)/devhelp/$(PKG) $$HOME/.local/share/devhelp/$(PKG)"
	@echo "# devhelp"

epub:	$(DEPS)
	$(SPHINXBUILD) -b epub $(ALLSPHINXOPTS) $(BUILDDIR)/epub/$(PKG)
	@echo
	@echo "Build finished. The epub file is in $(BUILDDIR)/epub/$(PKG)."

latex:	$(DEPS)
	$(SPHINXBUILD) -b latex $(ALLSPHINXOPTS) $(BUILDDIR)/latex
	@echo
	@echo "Build finished; the LaTeX files are in $(BUILDDIR)/latex."
	@echo "Run \`make' in that directory to run these through (pdf)latex" \
	      "(use \`make latexpdf' here to do that automatically)."

latexpdf: $(DEPS)
	$(SPHINXBUILD) -b latex $(ALLSPHINXOPTS) $(BUILDDIR)/latex
	@echo "Running LaTeX files through pdflatex..."
	$(MAKE) -C $(BUILDDIR)/latex all-pdf
	@echo "pdflatex finished; the PDF files are in $(BUILDDIR)/latex/sigticam-doc-$(lang).pdf"

text:	$(DEPS)
	$(SPHINXBUILD) -b text $(ALLSPHINXOPTS) $(BUILDDIR)/text/$(PKG)
	@echo
	@echo "Build finished. The text files are in $(BUILDDIR)/text/$(PKG)."

man:	$(DEPS)
	$(SPHINXBUILD) -b man $(ALLSPHINXOPTS) $(BUILDDIR)/man/$(PKG)
	@echo
	@echo "Build finished. The manual pages are in $(BUILDDIR)/man/$(PKG)."

texinfo: $(DEPS)
	$(SPHINXBUILD) -b texinfo $(ALLSPHINXOPTS) $(BUILDDIR)/texinfo/$(PKG)
	@echo
	@echo "Build finished. The Texinfo files are in $(BUILDDIR)/texinfo/$(PKG)."
	@echo "Run \`make' in that directory to run these through makeinfo" \
	      "(use \`make info' here to do that automatically)."

info:	$(DEPS)
	$(SPHINXBUILD) -b texinfo $(ALLSPHINXOPTS) $(BUILDDIR)/texinfo/$(PKG)
	@echo "Running Texinfo files through makeinfo..."
	make -C $(BUILDDIR)/texinfo info
	@echo "makeinfo finished; the Info files are in $(BUILDDIR)/texinfo/$(PKG)."

gettext: $(DEPS)
	$(SPHINXBUILD) -b gettext $(I18NSPHINXOPTS) $(BUILDDIR)/locale/$(PKG)
	@echo
	@echo "Build finished. The message catalogs are in $(BUILDDIR)/locale/$(PKG)."


changes: $(DEPS)
	$(SPHINXBUILD) -b changes $(ALLSPHINXOPTS) $(BUILDDIR)/changes/$(PKG)
	@echo
	@echo "The overview file is in $(BUILDDIR)/changes/$(PKG)."

linkcheck: $(DEPS)
	$(SPHINXBUILD) -b linkcheck $(ALLSPHINXOPTS) $(BUILDDIR)/linkcheck/$(PKG)
	@echo
	@echo "Link check complete; look for any errors in the above output " \
	      "or in $(BUILDDIR)/linkcheck/$(PKG)/output.txt."

doctest: $(DEPS)
	$(SPHINXBUILD) -b doctest $(ALLSPHINXOPTS) $(BUILDDIR)/doctest/$(PKG)
	@echo "Testing of doctests in the sources finished, look at the " \
	      "results in $(BUILDDIR)/doctest/$(PKG)/output.txt."

%-dist: % $(DEPS)
	@echo "> Making $@ $(BUILDDIR)/$(PKG).tar.gz"
	@$(TAR) -C $(BUILDDIR)/$* -czf $(BUILDDIR)/$(PKG).tar.gz \
		--anchored \
		--exclude $(PKG)/.buildinfo \
		--exclude $(PKG)/objects.inv \
		$(PKG)

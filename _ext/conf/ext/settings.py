import os

def noop():
    pass

def setup(app):
    if app.config.setup:
       app.config.setup(app)
       app.config.setup = None

    app.config.values['pkg'] = ('', '')
    app.config.values['author'] = ('', '')
    app.config.values['version'] = ('', '')
    app.config.init_values()
    app.config.init_values = noop

    settings_path = os.path.join(app.confdir, 'settings.py')
    if os.path.exists(settings_path):
       execfile(settings_path, app.config.__dict__)

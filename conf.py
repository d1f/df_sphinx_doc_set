import sys, os

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, os.path.abspath('./_ext'))

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
  #'sphinx.ext.intersphinx', # comment off to not download objects.inv
  'sphinx.ext.todo', 'sphinx.ext.coverage'
  ,'sphinx.ext.ifconfig'
  ,  'conf.ext.settings'
# , 'mkdog.ext.make'
]
